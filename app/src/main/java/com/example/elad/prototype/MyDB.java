package com.example.elad.prototype;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MyDB{
    String TAG = "DbHelper";

    private DatabaseHelper dbHelper;

    private SQLiteDatabase database;

    public final static String USERS_TABLE="MyUsers"; // name of table

    public final static String USER_GENDER="gender";  // name of employee
    public final static String USER_PART="part";
    public final static String USER_SYM="symptom";
    public final static String USER_LAT="lat";
    public final static String USER_LNG="lng";

    /**
     *
     * @param context
     */
    public MyDB(Context context){
        dbHelper = new DatabaseHelper(context);
        database = dbHelper.getWritableDatabase();
    }


    public void createRecords(String gender, String part, String sym, String lat, String lng){
        ContentValues values = new ContentValues();
        values.put(USER_GENDER, gender);
        values.put(USER_PART, part);
        values.put(USER_SYM, sym);
        values.put(USER_LAT, lat);
        values.put(USER_LNG, lng);
        database.insert(USERS_TABLE, null, values);

    }

    public Cursor selectRecords() {
        String[] cols = new String[] {USER_GENDER, USER_PART, USER_SYM, USER_LAT, USER_LNG};
        Cursor mCursor = database.query(true, USERS_TABLE,cols, null
                , null, null, null, null, null);
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor; // iterate to get each value.
    }


    /**
     * Helper function that parses a given table into a string
     * and returns it for easy printing. The string consists of
     * the table name and then each row is iterated through with
     * column_name: value pairs printed out.
     *
     * @return the table tableName as a string
     */
    public String getTableAsString() {
        Log.d(TAG, "getTableAsString called");
        String tableString = String.format("Table %s:\n", "MyUsers");
        Cursor allRows  = database.rawQuery("SELECT * FROM " + "MyUsers", null);
        if (allRows.moveToFirst() ){
            String[] columnNames = allRows.getColumnNames();
            do {
                for (String name: columnNames) {
                    tableString += String.format("%s: %s\n", name,
                            allRows.getString(allRows.getColumnIndex(name)));
                }
                tableString += "\n";

            } while (allRows.moveToNext());
        }

        return tableString;
    }
}