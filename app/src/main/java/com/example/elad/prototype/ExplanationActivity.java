package com.example.elad.prototype;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ExplanationActivity extends AppCompatActivity {
    TextView titleView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explanation);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar);

        // get extras from parts select activity
        Bundle extras = getIntent().getExtras();
        String symValue;
        if (extras != null) {
            symValue = extras.getString("sym");

            MainActivity.user.put("symptom", symValue);

            titleView = (TextView) findViewById(R.id.appTitle);
            titleView.setText(symValue);
        }
    }

    public void onClickFinish(View view) {
//        finish();
//        android.os.Process.killProcess(android.os.Process.myPid());
//        super.onDestroy();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void onClickShareLocation(View view) {
        Log.d("DbHelper", MainActivity.myDB.getTableAsString());

        Intent intent = new Intent(ExplanationActivity.this, MapsActivity.class);
        startActivity(intent);

    }
}
