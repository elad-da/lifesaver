package com.example.elad.prototype;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SelectSymptoms extends AppCompatActivity {

    TextView titleView;
    ListView listView;

    String[] symStringsHead ={
            "איבוד חוש הריח או הטעם",
            "בלוטות נפוחות בצוואר",
            "גירוד ללא פריחה",
            "הופעת סחרחורת חדשה",
            "חוסר שיווי משקל",
            "טיניטוס",
            "יובש פה וצמא מוגבר",
            "כאב גרון",
            "כאב צוואר",
            "כאב ראש",
            "כאבי לסת, חניכיים או שיניים",
            "קשיים בבליעה",
            "שיעול והתקררות",
            "שיעול מתמשך"
    };

    String[] symStringsEyes ={
            "בעיות בראייה",
            "הפרשות מהעיניים",
            "יובש בעיניים ובפה",
            "כאב בעיניים",
            "שיעול והתקררות"
    };

    String[] symStringsEarNose ={
            "איבוד שמיעה פתאומי",
            "בלוטות נפוחות בצוואר",
            "הופעת סחרחורת חדשה",
            "חוסר שיווי משקל",
            "טיניטוס",
            "יובש פה וצמא מוגבר",
            "יובש בעיניים ובפה",
            "כאב גרון",
            "כאב אוזניים בקרב מבוגרים",
            "כאב ראש",
            "כאבי לסת, חניכיים או שיניים",
            "קשיים בבליעה",
            "שיעול והתקררות",
            "שיעול מתמשך"
    };

    String[] symStringsChest ={
            "בלוטות נפוחות (כללי)",
            "גירוד ללא פריחה",
            "דפיקות לב (פלפיטציות)",
            "כאב בגב התחתון",
            "כאב בחזה",
            "כאב דיפוזי (מפושט)",
            "שיעול והתקררות",
            "שיעול מתמשך"
    };

    String[] symStringsArms ={
            "בלוטות נפוחות (כללי)",
            "גירוד ללא פריחה",
            "כאב בכף היד",
            "כאב בכתף",
            "כאב במרפק",
            "כאב בשורש כף היד",
            "רעד בכף היד",
            "תסמונת ריינו"
    };

    String[] symStringsStomach ={
            "איבוד שליטה על מתן שתן",
            "בחילה והקאה",
            "גירוד ללא פריחה",
            "דימום רקטלי",
            "דם בשתן",
            "כאבים, פצעים, הפרשות או גושים באיבר המין",
            "שלשול",
    };

    String[] symStringsLeg ={
            "הופעה חדשה של נפיחות ברגליים (בצקת)",
            "כאב בברך",
            "כאב בכפות הרגליים",
            "כאב בקרסול",
            "כאב דיפוזי (מפושט)",
            "כאבים במפרק הירך",
            "נפיחות מתמשכת בברך",
    };

    String[] symStringsOther ={
            "היעדר תחושה או עקצוץ",
            "טיפול בעור יבש",
            "ירידה בלתי מכוונת במשקל",
            "עייפות במשך היום",
            "עלייה בלתי מוסברת במשקל",
    };

    String[] chosenSymptom = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_symptoms);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar);

        listView = (ListView) findViewById(R.id.select_sym_list);

        String title = "בחר סימפטום";
        // get extras from parts select activity
        Bundle extras = getIntent().getExtras();
        String partValue;
        int partPosition;
        if (extras != null) {
            partValue = extras.getString("part");
            partPosition = extras.getInt("partPosition");
            chosenSymptom = null;
            switch(partPosition){

                case 0:
                    chosenSymptom = symStringsHead.clone();
                    break;
                case 1:
                    chosenSymptom = symStringsEyes.clone();
                    break;
                case 2:
                    chosenSymptom = symStringsEarNose.clone();
                    break;
                case 3:
                    chosenSymptom = symStringsChest.clone();
                    break;
                case 4:
                    chosenSymptom = symStringsArms.clone();
                    break;
                case 5:
                    chosenSymptom = symStringsStomach.clone();
                    break;
                case 6:
                    chosenSymptom = symStringsLeg.clone();
                    break;
                case 7:
                    chosenSymptom = symStringsOther.clone();
                    break;
                default:
                    break;
            }

            MainActivity.user.put("part", partValue);

            titleView = (TextView) findViewById(R.id.appTitle);
            titleView.setText(title + " - " + partValue);
        }



        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.my_list2,R.id.ItemName2, chosenSymptom);


        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(SelectSymptoms.this, ExplanationActivity.class);

                // ListView Clicked item index
                int itemPosition = position;

                //Create the bundle
                Bundle bundle = new Bundle();

                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);

                //Add your data to bundle
                bundle.putString("sym", itemValue);

                //Add the bundle to the intent
                intent.putExtras(bundle);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_SHORT)
                        .show();

                startActivity(intent);

            }

        });
    }
}
