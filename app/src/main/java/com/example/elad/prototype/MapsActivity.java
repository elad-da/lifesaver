package com.example.elad.prototype;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    LocationManager locationManager;

    double lat;
    double lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        String svcName = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) getSystemService(svcName);

        Criteria criteria = new Criteria();
        //String bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true));

        String provider = LocationManager.GPS_PROVIDER; //Replace with bestProvider

        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        //Location location = locationManager.getLastKnownLocation(provider);

        Location location = getLastKnownLocation();

//        lat=location.getLatitude();
//        lng=location.getLongitude();

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());


        MainActivity.user.put("lat", Double.toString(location.getLatitude()));
        MainActivity.user.put("lng", Double.toString(location.getLongitude()));

        if(!MainActivity.globalData.contains(MainActivity.user)) {
            MainActivity.globalData.add(MainActivity.user);

//            for(String name: MainActivity.user.keySet()){
//                String key = name;
//                String value = MainActivity.user.get(name);
//                Toast.makeText(this, key + " " + value, Toast.LENGTH_LONG).show();
//            }

            String gender = MainActivity.user.get("gender");
            String part = MainActivity.user.get("part");
            String sym = MainActivity.user.get("symptom");
            String lat = MainActivity.user.get("lat");
            String lng = MainActivity.user.get("lng");

            MainActivity.myDB.createRecords(gender, part, sym, lat, lng);
        }
        else
            MainActivity.user.clear();

        if (location != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
            mMap.addCircle(new CircleOptions().center(latLng).radius(1500).strokeColor(Color.RED).strokeWidth(3).fillColor(Color.argb(20, 0, 0, 255)));
            mMap.addMarker(new MarkerOptions().position(latLng).title("ישנם 4 חולים ברדיוס של 15 קמ")).showInfoWindow();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(this);
        if (location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14.0f));
            mMap.addCircle(new CircleOptions().center(latLng).radius(1500).strokeColor(Color.RED).strokeWidth(3).fillColor(Color.argb(20, 0, 0, 255)));
            mMap.addMarker(new MarkerOptions().position(latLng).title("ישנם 4 חולים ברדיוס של 15 קמ")).showInfoWindow();
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    // Private method return last known location
    private Location getLastKnownLocation() {
        LocationManager mLocationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = mLocationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            if (Build.VERSION.SDK_INT >= 23 &&
                    ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            Location l = mLocationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }
}
