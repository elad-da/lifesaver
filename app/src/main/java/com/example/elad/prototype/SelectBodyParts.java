package com.example.elad.prototype;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class SelectBodyParts extends AppCompatActivity {



    TextView titleView;
    ListView listView;
    String[] partsStrings ={
            "ראש וצוואר",
            "עיניים",
            "אף,אוזן,גרון",
            "חזה וגב",
            "זרועות וידיים",
            "בטן ואגן הירכיים",
            "רגליים",
            "אחר"
    };

    Integer[] imgid={
            R.drawable.head,
            R.drawable.eye,
            R.drawable.ear,
            R.drawable.backo,
            R.drawable.arm,
            R.drawable.stomach,
            R.drawable.leg,
            R.drawable.human_body,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_body_parts);


        //setTitle("בחר חלק בגוף");
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar);

        CustomListAdapter adapter=new CustomListAdapter(this, partsStrings, imgid);
        listView = (ListView) findViewById(R.id.select_parts_list);

        String title = "בחר חלק בגוף";
        // get extras from parts select activity
        Bundle extras = getIntent().getExtras();
        String genderValue;
        if (extras != null) {
            genderValue = extras.getString("gender");
            MainActivity.user.put("gender", genderValue);
            //textViewHeader.setText(textViewHeader.getText() + " - " + genderValue);
            titleView = (TextView) findViewById(R.id.appTitle);
            titleView.setText(title + " - " + genderValue);
        }


        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
//                R.layout.my_list,R.id.ItemName, partsStrings);


        // Assign adapter to ListView
        listView.setAdapter(adapter);

        // ListView Item Click Listener
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                Intent intent = new Intent(SelectBodyParts.this, SelectSymptoms.class);

                // ListView Clicked item index
                int itemPosition = position;

                //Create the bundle
                Bundle bundle = new Bundle();

                // ListView Clicked item value
                String itemValue = (String) listView.getItemAtPosition(position);

                //Add your data to bundle
                bundle.putString("part", itemValue);
                bundle.putInt("partPosition", position);

                //Add the bundle to the intent
                intent.putExtras(bundle);

                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "Position :" + itemPosition + "  ListItem : " + itemValue, Toast.LENGTH_SHORT)
                        .show();

                startActivity(intent);
            }

        });
    }
}
