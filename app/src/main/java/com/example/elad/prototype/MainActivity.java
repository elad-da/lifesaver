package com.example.elad.prototype;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    static List<HashMap<String, String>> globalData = new ArrayList<HashMap<String, String>>();
    static HashMap<String, String> user = new HashMap<String, String>();

    static MyDB myDB;
    EditText age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar);


        /***Parse Database***/
        // https://parse.com/docs/android/guide#local-datastore
        Parse.enableLocalDatastore(this);
        Parse.initialize(this);

        /***SQLite Database***/
        myDB = new MyDB(this);

        age = (EditText) findViewById(R.id.age_edit_text);

        ParseObject testObject = new ParseObject("TestObject");
        testObject.put("foo", "bar");
        testObject.saveInBackground();

    }

    public void onClickWoman(View view) {
        Intent intent = new Intent(MainActivity.this, SelectBodyParts.class);
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("gender","אישה");

        //Add the bundle to the intent
        intent.putExtras(bundle);

        if( age.getText().toString().trim().equals("")){

            //Toast.makeText(this, "אנא הכנס גיל", Toast.LENGTH_SHORT).show();

            age.setError( "אנא הכנס גיל" );

        }
        else{
            startActivity(intent);
            Toast.makeText(this, "You Picked Woman", Toast.LENGTH_SHORT).show();
        }

    }


    public void onClickMan(View view) {

        Intent intent = new Intent(MainActivity.this, SelectBodyParts.class);
        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("gender", "גבר");

        //Add the bundle to the intent
        intent.putExtras(bundle);
        if( age.getText().toString().trim().equals("")){

            //Toast.makeText(this, "אנא הכנס גיל", Toast.LENGTH_SHORT).show();

            age.setError("אנא הכנס גיל" );

        }
        else{
            startActivity(intent);
            Toast.makeText(this, "You Picked Man", Toast.LENGTH_SHORT).show();
        }
    }
}
